package com.decathlon.plugin.decathlonservices;

import com.decathlon.android.decathlonservices.available.AvailableManager;
import com.decathlon.android.decathlonservices.available.model.AvailableParcel;
import com.decathlon.android.decathlonservices.available.model.AvailableWithDownload;
import com.decathlon.android.decathlonservices.common.ServiceAvailableCallback;
import com.decathlon.android.decathlonservices.common.ServiceAvailableError;
import com.decathlon.android.decathlonservices.common.ServiceCallback;
import com.decathlon.android.decathlonservices.common.ServiceError;
import com.decathlon.android.decathlonservices.login.LoginManager;
import com.decathlon.android.decathlonservices.login.model.User;
import com.decathlon.android.decathlonservices.log.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import android.content.Context;
import android.content.DialogInterface;
import android.content.ComponentName;
import android.content.Intent;

public class DecathlonServicesPlugin extends CordovaPlugin {
    private static final String TAG = "Trocathlon";
    private LoginManager loginManager;
    private AvailableManager availableManager;
    public static final String DS_PACKAGE = "com.decathlon.android.decathlonservices";
    public static final String DS_DOWNLOAD_ACTIVITY = "com.decathlon.android.decathlonservices.mam.ui.DownloadActivity";

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.init(cordova.getActivity().getApplicationContext());
    }

    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if ("getToken".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    getToken(callbackContext);
                }
            });

            return true;
        }

        if ("getCurrentToken".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    getCurrentToken(callbackContext);
                }
            });

            return true;
        }

        if ("getUser".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    getUser(callbackContext);
                }
            });

            return true;
        }

        if ("logout".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    logout(callbackContext);
                }
            });

            return true;
        }

        if ("checkState".equals(action)) {
            final String appId = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    available(callbackContext, appId);
                }
            });
            return true;
        }

        if ("getAvailableInformationWithDownload".equals(action)) {
            final String appId = args.getString(0);
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    getAvailableInformationWithDownload(callbackContext, appId);
                }
            });

            return true;
        }

        if ("log".equals(action)) {
            final String level = args.getString(0);
            final String log = args.getString(1);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    log(level, log);
                }
            });

            return true;
        }

        return false;
    }

    private void getToken(final CallbackContext callback) {
        final JSONObject authenticationResult = new JSONObject();

        if (loginManager == null) {
            loginManager = new LoginManager(cordova.getActivity().getApplicationContext());
        }

        loginManager.getToken(true, new ServiceCallback<String>() {
            @Override
            public void success(String token) {
                try {
                    if(token == null || token.isEmpty()) {
                        Log.e("DecathlonServicesPlugin", "Token retrieve from DecathlonService is null or empty");
                        callback.error("Token retrieve from DecathlonService is null or empty");

                        return;
                    }

                    authenticationResult.put("token", token);
                    callback.success(authenticationResult); // Thread-safe.
                } catch (JSONException e) {
                    if (e.getMessage() == null || e.getMessage().isEmpty()) {
                        Log.i("DecathlonServicesPlugin", "A JsonException occured");
                        callback.error("A JsonException occured");
                    } else {
                        Log.i("DecathlonServicesPlugin", e.getMessage());
                        callback.error(e.getMessage());
                    }
                }
            }

            @Override
            public void failure(ServiceError serviceError) {
                Log.i("DecathlonServicesPlugin", serviceError.getErrorMessage());
                callback.error(serviceError.getErrorMessage());
            }
        });
    }

    private void getCurrentToken(final CallbackContext callback) {
        final JSONObject result = new JSONObject();

        if (loginManager == null) {
            loginManager = new LoginManager(cordova.getActivity().getApplicationContext());
        }

        try {
            result.put("token", loginManager.getCurrentToken());
            callback.success(result); // Thread-safe.
        } catch (JSONException e) {
            Log.i("DecathlonServicesPlugin", e.getMessage());
            callback.error(e.getMessage());
        }
    }

    private void getUser(final CallbackContext callback) {
        final JSONObject authenticationResult = new JSONObject();

        if (loginManager == null) {
            loginManager = new LoginManager(cordova.getActivity().getApplicationContext());
        }

        loginManager.getUser(new ServiceCallback<User>() {
            @Override
            public void success(User user) {
                try {
                    if (user == null) {
                        Log.e("DecathlonServicesPlugin", "User object retrieve from DecathlonService is null");
                        callback.error("User object retrieve from DecathlonService is null");

                        return;
                    }

                    authenticationResult.put("language",  user.getPreferedLanguage());
                    authenticationResult.put("token", user.getToken());
                    authenticationResult.put("tokenWithBearer", user.getTokenWithBearer());
                    callback.success(authenticationResult); // Thread-safe.
                } catch (JSONException e) {
                    callback.error(e.getMessage());
                    Log.i("DecathlonServicesPlugin", e.getMessage());
                }
            }

            @Override
            public void failure(ServiceError serviceError) {
                Log.i("DecathlonServicesPlugin", serviceError.getErrorMessage());
                callback.error(serviceError.getErrorMessage());
            }
        });
    }

    private void logout(final CallbackContext callback) {
        if (loginManager == null) {
            loginManager = new LoginManager(cordova.getActivity().getApplicationContext());
        }

        loginManager.logout();
        Log.i("DecathlonServicesPlugin", "Process logout from Decathlon Services");
        callback.success();
    }

    private void available(final CallbackContext callback, final String appId) {
        if (availableManager == null) {
            availableManager = new AvailableManager(cordova.getActivity().getApplicationContext());
        }

        availableManager.getAvailableInformation(appId, new ServiceAvailableCallback<AvailableParcel>() {
            @Override
            public void success(AvailableParcel availableParcel) {
                try {
                    JSONObject json = new JSONObject();
                    json.put("state", availableParcel.getApkState());
                    json.put("version", availableParcel.getVersion());
                    callback.success(json);
                } catch (JSONException e) {
                    callback.error(e.getMessage());
                    Log.e("DecathlonServicesPlugin", e.getMessage());
                }
            }

            @Override
            public void failure(ServiceAvailableError serviceAvailableError) {
                Log.i("DecathlonServicesPlugin", serviceAvailableError.getErrorMessage());
                callback.error(serviceAvailableError.getErrorMessage());
            }
        });
    }

    private void getAvailableInformationWithDownload(final CallbackContext callback, final String appId) {
        if (availableManager == null) {
            availableManager = new AvailableManager(cordova.getActivity().getApplicationContext());
        }

        availableManager.getAvailableInformationWithDownload(appId, new ServiceAvailableCallback<AvailableWithDownload>() {
            @Override
            public void success(AvailableWithDownload availableParcel) {
                if(availableParcel.isVersionOk()) {
                    try {
                        JSONObject json = new JSONObject();
                        json.put("version_ok", true);
                        callback.success(json);
                    } catch (JSONException e) {
                        callback.error(e.getMessage());
                        Log.e("DecathlonServicesPlugin", e.getMessage());
                    }
                } else {
                    try {
                        JSONObject json = new JSONObject();
                        json.put("version_ok", false);
                        callback.success(json);
                    } catch (JSONException e) {
                        callback.error(e.getMessage());
                        Log.e("DecathlonServicesPlugin", e.getMessage());
                    }
                }
            }

            @Override
            public void failure(ServiceAvailableError serviceAvailableError) {
                Log.i("DecathlonServicesPlugin", serviceAvailableError.getErrorMessage());
                callback.error(serviceAvailableError.getErrorMessage());
            }
        }, false);
    }

    public void log(final String level, final String log) {
        String levelLower = level.toLowerCase();

        if (levelLower.equals("error")) {
            Log.e(TAG, log);
        } else if (levelLower.equals("info")) {
            Log.i(TAG, log);
        } else if (levelLower.equals("debug")) {
            Log.d(TAG, log);
        } else if (levelLower.equals("verbose")) {
            Log.v(TAG, log);
        } else if (levelLower.equals("warn") ){
            Log.w(TAG, log);
        } else {
            Log.i(TAG, log);
        }
    }
}
