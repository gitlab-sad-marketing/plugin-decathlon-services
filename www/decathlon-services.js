function DecathlonServices() {

}

DecathlonServices.prototype.getToken = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "getToken", []);
};

DecathlonServices.prototype.getCurrentToken = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "getCurrentToken", []);
};

DecathlonServices.prototype.getUser = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "getUser", []);
};

DecathlonServices.prototype.logout = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "logout", []);
};

DecathlonServices.prototype.checkState = function(successCallback, errorCallback, appId) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "checkState", [appId]);
};

DecathlonServices.prototype.getAvailableInformationWithDownload = function(successCallback, errorCallback, appId) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "getAvailableInformationWithDownload", [appId]);
};

DecathlonServices.prototype.log = function(level, log) {
    cordova.exec(null, null, "DecathlonServicesPlugin", "logInfo", [level, log]);
};

module.exports = new DecathlonServices();